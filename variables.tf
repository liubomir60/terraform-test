variable "resource_group_name" {
  default = "rg-tf-test"
}

variable "location" {
  default = "eastus"
}